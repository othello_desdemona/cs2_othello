//
// Exampleplayer.h
//
// Revision History
//     3/11/13  Ben Lieber      Added MoveAndScore struct  
//     3/17/13  Ben Lieber      Got rid of unncessary functions    
//

#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <iostream>
#include <cassert>

#include "common.h"
#include "masks.h"
#include "table_lookup.h"

using namespace std;

struct MoveAndScore {
    Move* move;
    int score;
};


class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
     
    bool occupied(int x, int y);        
    bool isDone();

    int count(Side side);
    int countBlack();
    int countWhite();
    int countAll();
    bitset<64> getBoardPieces(Side side);

    bool hasMoves(Side side);
    int doMove(Move *m, Side side, bool useWeighing = false);
    int addScoreWeight(Side side, Board *bOriginal);
    int getNumValidMoves(Side side);
};

#endif
