//
// board.cpp
//
// Revision History
//     3/11/13  Ben Lieber      Added bestGetMove function
//     3/11/13  Ben Lieber      Modified doMove function for speed and to return
//                                score from performing move
//     3/12/13  Ben Lieber      Added score weighing, added helper function  
//     3/12/13  Ben Lieber      Attempted to optimize score weighing heuristic
//                                against BetterPlayer
//     3/17/13  Ben Lieber      Split score weighing into its own function
//                              Got rid of unnecessary functions
//     3/18/13  Ben Lieber      Added getNumValidMoves function
//

#include "board.h"


/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Count all taken stones.
 */
int Board::countAll() {
    return taken.count();
}

/*
 * Returns 64 bit array corresponding to board pieces of given side
 */
bitset<64> Board::getBoardPieces(Side side) {
    if(side == BLACK) {
        return black;
    }
    else {
        return taken ^ black;
    }
}


/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    // FIX OR GET RID OFF
    return false;
}


/*
 * Modifies the board to reflect the specified move. Assumes argument move is
 * valid, if pointer is NULL this may result in a segmentation fault! Also
 * assumes move space will not be occupied - this too may result in error. 
 * Returns score corresponding to move. If score is 0, this implies move is 
 * invalid.
 *
 * It is also recommended to apply simple checks to see if a move will be valid
 * before calling this function to improve speed.
 */
int Board::doMove(Move *m, Side side, bool useWeighing) {

    // Assert Block
    assert(m);                  // make sure move is valid

    // Attempt to perform move
    int X = m->getX();
    int Y = m->getY();

    // Copy original board
    Board *bOriginal = this->copy();

    // Otherwise continue to attempt move (look at neighbors)
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = DX_START[X]; dx <= DX_END[X]; dx++) {
        for (int dy = DY_START[Y]; dy <= DY_END[Y]; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Flip pebbles, update score
            Board *bCopy = this->copy();
            int x = X + dx;
            int y = Y + dy;
            while (onBoard(x, y) && get(other, x, y)) {
                bCopy->set(side, x, y);
                x += dx;
                y += dy;
            }

            // Only get to flip pebbles if flanked by pebble of given
            // side
            if (onBoard(x, y) && get(side, x, y)) {
                // Update board
                this->black = bCopy->black;
                this->taken = bCopy->taken;
                delete bCopy;
            }
        }
    }

    // Get basic score of move
    int score = this->count(side) - bOriginal->count(side);
    // And now set pebble to be moved (want to consider for additional score 
    // weighing but don't want for basic score)

    set(side, X, Y);

    // Additional score weighing
    // Only add if:
    //   -- Score is nonzero (i.e. move is valid in first place)
    //   -- Use weighing flag is set
    if((score > 0) && useWeighing) {
        score += this->addScoreWeight(side, bOriginal);
        // Don't want to return 0 score as this implies invalid move
        if(score == 0) {
            score = -1;
        }
    }

    // Cleanup
    delete bOriginal;

    // And return score
    return score;
}

// Score weighting helper function
int Board::addScoreWeight(Side side, Board *bOriginal) {  
    bitset<64> b = this->getBoardPieces(side) &
                   ~(bOriginal->getBoardPieces(side));
    int score = 0;

    // Corners
    score += 5 * (b & corners).count();
    // Corner Neighbors (not including diagonals):
    score += -10 * (b & edge_corner_nbrs).count();
    // Corner Neighbors (diagonals)
    score += -10 * (b & diag_corner_nbrs).count(); 
    // Edges (excluding ones next to corners)
    score += 3 * (b & edges).count();
    // Edges Neighbors
    score += -1 * (b & edge_nbrs).count();

    return score;
}

/*
 * Return number of valid moves on a board.
 */
 
int Board::getNumValidMoves(Side side) {
    int num_moves = 0;
    Side other = (side == BLACK) ? WHITE : BLACK;

    for (int X = 0; X < 8; X++) {
        for (int Y = 0; Y < 8; Y++) {
            // If space occupied can't do anything so just skip the rest below
            if (this->occupied(X, Y)) continue;
            
            // Quick check: if no neighbors of other color around just return 0.
            // Apply mask to other pieces - if no neighbors around will get empty board.
            if ((this->getBoardPieces(other) & neighbors[X + 8*Y]) == emptyBoard) {
                continue;
            }

            // Complete check: attempt move
            Board *bCopy = this->copy();
            Move *move = new Move(X,Y);
            if(bCopy->doMove(move, side)) num_moves++;
            delete move;
            delete bCopy;
        }
    }

    return num_moves;
}