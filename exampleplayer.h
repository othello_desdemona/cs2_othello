#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <ctime>
#include <list>

#include "common.h"
#include "board.h"


class ExamplePlayer {
private:
	Board* b;           //an instance of the board class
	Side s;             //side we are on (either WHITE or BLACK)
	Side opp_s;         //the opponents side (opposite of our s)
    int X, Y;           //hold onto move indices
    int bestScore;      //contains minimax score
    int moveScore;      //contains score of move (may be based on deeper best scores)
    ExamplePlayer *deepP; // double linked list functionality
    ExamplePlayer *prevP; 

public:
    // Constructors
    ExamplePlayer(Side side);
    ExamplePlayer *copy(Side side);
    // Destructor
    ~ExamplePlayer() { delete b; delete deepP; }

    // Add node
    void addDeepPlayer(Board *b, Side s, int X=-1, int Y=0) {
        this->deepP = new ExamplePlayer(s);
        deepP->b = b;
        deepP->X = X;
        deepP->Y = Y;
        deepP->prevP = this;
        deepP->deepP = NULL;
    }

    // Time calibration
    void timeCalibrate();

    // Other move decision related functions
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *getBestMove();
    void getLastEffortDepth(int msLeft);
    int makeMove(Board *b);

};

#endif
