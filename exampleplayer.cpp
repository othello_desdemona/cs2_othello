//
// Exampleplayer.cpp (Desdemona)
//
// Revision History
//     --       Jeff Han        Initial Revision
//     3/11/13  Ben Lieber      Updated doMove algorithm to make better decision.
//     3/11/13  Ben Lieber      Added getBestMove member function
//     3/12/13  Ben Lieber      Modified algorithm to employ extra board depth
//                                when board is nearly full, turn off score
//                                weighing for nearly full board or other
//                                players move considerations
//     3/17/13  Ben Lieber      Removed unnecessary move data member of 
//                                ExamplePlayer.
//     3/19/13  Ben Lieber      Moved some checks from doMove into getBestMove
//                                in hopes of improving speed.
//                              Added timing calibration and related routines,
//                                shared variables.
//  
// 

#include "exampleplayer.h"


// Default recursion depth of board search algorithm
const int BOARD_DEPTH = 2;
static int depth;
// Depth board searches at end of game depending on calibration
static int last_effort_depth;
// If have this much time left (ms) move as quickly as possible.
const int PANIC_TIME = 2000;


// Used for calibration; counts number of time getBestMove can be called
// in given time frame.
static unsigned long int calls_count;
// How long (seconds) to calibrate
const float CALIBRATE_TIME = 10;
// Estimated of number of moves that can be executed per s
static float scans_per_ms;
// Print calibration specs
const bool PRINT_SPECS = true;


// Initialization value for best score member. As score can be negative, set
// this to be really low.
const int NO_MOVES_SCORE = -1000;


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

    this->b = new Board();           //create a new board
    this->s = side;                  //set the side we are one
    if (this->s == WHITE){           //set opponenets side
       this->opp_s = BLACK;
    }
    else {
       this->opp_s = WHITE;
    }
    this->bestScore = NO_MOVES_SCORE; // Assume no moves default
    this->moveScore = NO_MOVES_SCORE; // Assume no moves default
    this->deepP = NULL;

    // Only calibrate time on first initialization:
    if(!calls_count) this->timeCalibrate();
}

/*
 * Copy constructor
 */
ExamplePlayer *ExamplePlayer::copy(Side side) {
    ExamplePlayer *newPlayer = new ExamplePlayer(side);
    newPlayer->b = b->copy();
    return newPlayer;
}


/*
 * Calibrates timing of algorithm on system. Updates scans_per_ms, depth, and
 * calls_count shared variables.
 */
void ExamplePlayer::timeCalibrate() {
    // So this wont be called again
    calls_count++;

    // Don't want to modify object itself
    ExamplePlayer *Pcopy = this->copy(s);
    Pcopy->getBestMove();

    // Seed the board
    int SEED_DEPTH = 30;   // make this many moves before iterating
    depth = 0;             // don't need any recursion depth
    Side seed_side = Pcopy->s;

    for(int i = 0; i < SEED_DEPTH; i++) {
        Move *move = Pcopy->getBestMove();

        // Add the move to the board
        if(move) {
            (Pcopy->b)->doMove(move, seed_side);  
            seed_side = (seed_side == BLACK) ? WHITE : BLACK;                          
        }

        // Cleanup
        delete move;
    }

    
    // Now calibrate, counting number of calls at 0 depth.
    clock_t start = time(0);

    while(time(0) - start < CALIBRATE_TIME) {
        calls_count++;                     // Increase count
        Move *move = Pcopy->getBestMove(); // Run this chump
        delete move;                       // Cleanup
    }

    // Now can set depth to default value
    depth = BOARD_DEPTH;   

    // And get number of scans per ms
    scans_per_ms = (calls_count / CALIBRATE_TIME) / 1000;

    // And print if desired
    if(PRINT_SPECS) {
        cerr << "Executed " << calls_count << " calls in " 
                            << CALIBRATE_TIME << " seconds." << endl;
        cerr << "scans per ms: " << scans_per_ms << endl << endl;
    }

    // Cleanup
    delete Pcopy;

}




/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {

    // Last move possible checked.
    if(opponentsMove) {                          //add opponents move to our board if it is not NULL
        b->doMove(opponentsMove, this->opp_s);   //since we have to update our board ourselves     
    }
    
    // Get last_effort_depth, print specs (do this only once initially)
    if(!last_effort_depth) {
        this->getLastEffortDepth(msLeft);
    }

    // Set depth for this iteration
    if(last_effort_depth + 1 >= 64 - b->countAll()) {
        depth = last_effort_depth;
    }

    // Panic Mode
    if((last_effort_depth <= BOARD_DEPTH) || msLeft < PANIC_TIME) {
        depth = 0;
    }

    // Get best move
    Move *move = this->getBestMove();

    // Add the move to the board
    if(move) {
        b->doMove(move, s);                            
    }

    // Return the move 
    return move;                            
}


/*
 * Last Effort Depth helper function
 */
void ExamplePlayer::getLastEffortDepth(int msLeft) {
    // We have to make a few assumptions on how many combinations we are
    // dealing with and thus how deeply we can look ahead at end of game.
    //
    // Beginning Assumptions:
    //   Board handles invalid moves quickly enough that we can ignore them.
    //
    // Worst case scenario:
    //   All moves are possible. We then have 1*2*3*4*...*n = n!
    //   cobinations.
    //
    // Average case:
    //   Lets say only valid_moves of moves are valid. Then we actually only
    //   have valid_moves! combinations. We approximate this using 
    //   VALID_MOVES_PER^n * n when we don't have the number of valid moves.
    //
    // We also have to remember that we still have to run n-1 to 1 times
    // once we hit last_effort depth. So we will include these in the total
    // number of moves.
    float VALID_MOVES_PER = .9;
    unsigned long int num_moves = 1; 
    unsigned int OVERHEAD = 10000;   // Account for some overhead
    float SAFETY_MUL = 3;            // Safety multiplier buffer
    unsigned long int moves_cap;

    // Get total number of moves allowed
    moves_cap = msLeft * scans_per_ms;

    // See how deep we can get
    while(true) {
        // Assume can do first move (num_moves set to 1 to start)
        last_effort_depth++;

        // Update number of moves to next iteration
        num_moves += num_moves * (last_effort_depth + 1) * VALID_MOVES_PER;
            
        // See if we are past moves limit
        if((num_moves + OVERHEAD) * SAFETY_MUL > moves_cap) { 
            num_moves /= (last_effort_depth++); // restore old num_moves
            break;
        }
    }

    // And print if desired
    if(PRINT_SPECS) {
        cerr << "moves cap: " << moves_cap << endl;
        cerr << "estimated number of moves: " << num_moves << endl;
        cerr << "last effort depth: " << last_effort_depth << endl << endl;
    }
}


/*
 * Returns best move based on depth shared variable
 */
Move *ExamplePlayer::getBestMove() {
    int depth_cntr = 0; 

    // Board Members
    this->X = -1;
    this->Y = 0;
    this->bestScore = NO_MOVES_SCORE;
    this->moveScore = NO_MOVES_SCORE;
            
    // Local Variables
    ExamplePlayer *Ppntr = NULL; // points to an example player
    Move *bestMove = NULL; // Save best move.

    while(true) {

        // This is the only Player standing
        if(Ppntr == NULL || Ppntr == this)
        {

            // Update best score and save move
            if(this->bestScore < this->moveScore) {
                delete bestMove;
                bestMove = new Move(X, Y);
                this->bestScore = this->moveScore;
            }

            // We have result
            if(this->Y == 7 && this->X == 7) {
                return bestMove;
            }

            // We don't have result: update move indices
            if(this->X == 7) {
                this->X = 0;
                (this->Y)++;
            }
            else {
                (this->X)++;
            }

            // And attempt to make move at indices
            Board *bCopy = b->copy();
            int score = this->makeMove(bCopy);
            if(score == 0) continue;      // Nope, move invalid
            else this->moveScore = score; // Ah score was valid. Lets give it a shot.

            // If we have nontrivial recursion depth, then attempt below:
            if(depth) {
                this->addDeepPlayer(bCopy, this->opp_s); // Add search layer
                Ppntr = this->deepP;                 // Update active layer
                depth_cntr++;                        // Update layer cntr
            }
            
        }

        
        // Here we hare looking at a deeper player
        else {

            // Update best score
            if(Ppntr->bestScore < Ppntr->moveScore) {
                Ppntr->bestScore = Ppntr->moveScore;
            }
            
            // We have result
            if(Ppntr->Y == 7 && Ppntr->X == 7) {                
                // Check and make sure at least one move was valid. Then can
                // update move score of layer above. Otherwise do nothing.
                if(!(Ppntr->bestScore == NO_MOVES_SCORE))
                    (Ppntr->prevP)->moveScore -= Ppntr->bestScore;

                // Cleanup and move to layer above
                Ppntr = Ppntr->prevP;     // Update active layer
                delete Ppntr->deepP;      // Get rid of old layer
                Ppntr->deepP = NULL;
                depth_cntr--;             // Update layer counter
                continue;
            }

            // We don't have result: update move indices
            if(Ppntr->X == 7) {
                Ppntr->X = 0;
                (Ppntr->Y)++;
            }
            else {
                (Ppntr->X)++;
            }

            // And attempt to make move at indices
            Board *bCopy = b->copy();
            int score = Ppntr->makeMove(bCopy);
            if(score == 0) continue;           // Nope, move invalid
            else Ppntr->moveScore = score;     // Ah score was valid. Lets give it a shot.

            // If recursion depth remaining add new layer
            if(depth_cntr < depth) {
                Ppntr->addDeepPlayer(bCopy, Ppntr->opp_s); // Add search layer
                Ppntr = Ppntr->deepP;                      // Update active layer
                depth_cntr++;                              // Update layer cntr
            }
            
        }

    }
}


/*
 * Attempts to make move based on coordinates, side members of player. Updates 
 * passed board. Returns score of move.
 */
int ExamplePlayer::makeMove(Board *board) {

    // If space occupied can't do anything so just return 0
    if (board->occupied(X, Y)) return 0;
    
    // Quick check: if no neighbors of other color around just return 0.
    // Apply mask to other pieces - if no neighbors around will get empty board.
    if ((board->getBoardPieces(opp_s) & neighbors[X + 8*Y]) == emptyBoard) {
        return 0;
    }
    
    // Thorough check: actually try to update board now.

    // Define move
    Move *move = new Move(X, Y);
 
    // Get score of doing move
    // Do not weigh if can see to end of game or if considering other
    //   players moves (depth is odd)
    bool useWeighing = true;
    if((board->countAll() > 64 - depth - 1) || (depth & 1)) {
        useWeighing = false;
    }
    int score = board->doMove(move, s, useWeighing);
    
    // Cleanup and return
    delete move;
    return score;
}