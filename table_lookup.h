//
// table_lookup.h
//
// Contains table_lookup for quick board operations. For constants relating to
// board layout specifically however, see masks.h
//
// Revision History
//     3/18/13  Ben Lieber      Initial revision
//

#ifndef __TABLE_LOOKUP_H__
#define __TABLE_LOOKUP_H__

// Factorial lookup table:
const unsigned long int factorial [] = {
    1,
    1,
    2,
    6,
    24,
    120,
    720,
    5040,
    40320,
    362880,
    3628800,
    39916800,
    479001600,
    6227020800,
    87178291200,
    1307674368000,
    20922789888000,
    355687428096000,
    6402373705728000,
    121645100408832000
};


// Lookup for what directions to look when attempting to make move and update
// board.
// 
// Rational:
//      If we are on edge (or one row/column inside edge) we can save time
//      by not looking in all directions, as we know it is not possible to turn
//      pebbles in the directions in which the board ends. Rather then check
//      in an IF statement, we can do this faster using table lookup.

const int DX_START[8] = {
    0,  // X = 0 -- cannot take pebbles to left
    0,  // X = 1 -- cannot take pebbles to left
    -1, // X = 2
    -1, // X = 3
    -1, // X = 4
    -1, // X = 5
    -1, // X = 6
    -1  // X = 7
};

const int DX_END[8] = {
    1,  // X = 0 
    1,  // X = 1 
    1,  // X = 2
    1,  // X = 3
    1,  // X = 4
    1,  // X = 5
    0,  // X = 6 -- cannot take pebbles to right
    0   // X = 7 -- cannot take pebbles to right
};

const int DY_START[8] = {
    0,  // Y = 0 -- cannot take pebbles in above direction
    0,  // Y = 1 -- cannot take pebbles in above direction
    -1, // Y = 2
    -1, // Y = 3
    -1, // Y = 4
    -1, // Y = 5
    -1, // Y = 6
    -1  // Y = 7
};

const int DY_END[8] = {
    1,  // Y = 0 
    1,  // Y = 1 
    1,  // Y = 2
    1,  // Y = 3
    1,  // Y = 4
    1,  // Y = 5
    0,  // Y = 6 -- cannot take pebbles in below direction
    0   // Y = 7 -- cannot take pebbles in below direction
};

#endif

