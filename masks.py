for y in range(8):
	for x in range(8):

		# Get board to print
		board = ['0'] * 64
		for dx in [-1, 0, 1]:
			for dy in [-1, 0, 1]:
				# outmost shell of board
				if x == 0 or x == 7 or y == 0 or y == 7:
					if dx == dy and dy == 0:
						pass
					elif dx + x > 7 or dx + x < 0:
						pass
					elif dy + y > 7 or dy + y < 0:
						pass
					else:
						board[(x + dx) + 8*(y +dy)] = '1'
				# can actually reduce mask size for squares just inside
				# the outermost shell
				else:
					if dx == dy and dy == 0:
						pass
					elif dx + x > 6 or dx + x < 1:
						pass
					elif dy + y > 6 or dy + y < 1:
						pass
					else:
						board[(x + dx) + 8*(y +dy)] = '1'
		board = board[::-1]

		# Now print
		print "// Neighbors of", "X =", x, ", Y =", y
		print "bitset<64> ("
		for start in range(0, 56, 8):
			end = start + 8
			print "string(\"" + "".join(board[start:end]) + "\") + "
		print "string(\"" + "".join(board[56:64]) + "\")"
		print "),"
		print




