//
// masks.h
//
// Contains masks for quick board operations. For other table lookup see
// table_lookup.
//
// Revision History
//     3/17/13  Ben Lieber      Initial revision
//

#ifndef __MASKS_H__
#define __MASKS_H__

#include <bitset>

using namespace std;

// Board of all 0's (empty)
const bitset<64> emptyBoard;

// Masks for score weighing
const bitset<64> corners (
    string("10000001") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("10000001")
);

const bitset<64> diag_corner_nbrs (
    string("00000000") + 
    string("01000010") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("01000010") + 
    string("00000000")
);

const bitset<64> edge_corner_nbrs (
    string("01000010") + 
    string("10000001") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("00000000") + 
    string("10000001") + 
    string("01000010")
);

const bitset<64> edges (
    string("00111100") + 
    string("00000000") + 
    string("10000001") + 
    string("10000001") + 
    string("10000001") + 
    string("10000001") + 
    string("00000000") + 
    string("00111100")
);

const bitset<64> edge_nbrs (
    string("00000000") + 
    string("00111100") + 
    string("01000010") + 
    string("01000010") + 
    string("01000010") + 
    string("01000010") + 
    string("00111100") + 
    string("00000000")
);

// Get neighbors for checking valid moves -- index list by x + 8 * y
const bitset<64> neighbors[64] = 
{

// Neighbors of X = 0 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000011") + 
string("00000010")
),

// Neighbors of X = 1 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000111") + 
string("00000101")
),

// Neighbors of X = 2 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00001110") + 
string("00001010")
),

// Neighbors of X = 3 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00011100") + 
string("00010100")
),

// Neighbors of X = 4 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00111000") + 
string("00101000")
),

// Neighbors of X = 5 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01110000") + 
string("01010000")
),

// Neighbors of X = 6 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("11100000") + 
string("10100000")
),

// Neighbors of X = 7 , Y = 0
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("11000000") + 
string("01000000")
),

// Neighbors of X = 0 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000011") + 
string("00000010") + 
string("00000011")
),

// Neighbors of X = 1 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000110") + 
string("00000100") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00001110") + 
string("00001010") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00011100") + 
string("00010100") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00111000") + 
string("00101000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01110000") + 
string("01010000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01100000") + 
string("00100000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 1
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("11000000") + 
string("01000000") + 
string("11000000")
),

// Neighbors of X = 0 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000011") + 
string("00000010") + 
string("00000011") + 
string("00000000")
),

// Neighbors of X = 1 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000110") + 
string("00000100") + 
string("00000110") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00001110") + 
string("00001010") + 
string("00001110") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00011100") + 
string("00010100") + 
string("00011100") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00111000") + 
string("00101000") + 
string("00111000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01110000") + 
string("01010000") + 
string("01110000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01100000") + 
string("00100000") + 
string("01100000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 2
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("11000000") + 
string("01000000") + 
string("11000000") + 
string("00000000")
),

// Neighbors of X = 0 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000011") + 
string("00000010") + 
string("00000011") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 1 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000110") + 
string("00000100") + 
string("00000110") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00001110") + 
string("00001010") + 
string("00001110") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00011100") + 
string("00010100") + 
string("00011100") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00111000") + 
string("00101000") + 
string("00111000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01110000") + 
string("01010000") + 
string("01110000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("01100000") + 
string("00100000") + 
string("01100000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 3
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("11000000") + 
string("01000000") + 
string("11000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 0 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000011") + 
string("00000010") + 
string("00000011") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 1 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00000110") + 
string("00000100") + 
string("00000110") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00001110") + 
string("00001010") + 
string("00001110") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00011100") + 
string("00010100") + 
string("00011100") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("00111000") + 
string("00101000") + 
string("00111000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("01110000") + 
string("01010000") + 
string("01110000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("01100000") + 
string("00100000") + 
string("01100000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 4
bitset<64> (
string("00000000") + 
string("00000000") + 
string("11000000") + 
string("01000000") + 
string("11000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 0 , Y = 5
bitset<64> (
string("00000000") + 
string("00000011") + 
string("00000010") + 
string("00000011") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 1 , Y = 5
bitset<64> (
string("00000000") + 
string("00000110") + 
string("00000100") + 
string("00000110") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 5
bitset<64> (
string("00000000") + 
string("00001110") + 
string("00001010") + 
string("00001110") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 5
bitset<64> (
string("00000000") + 
string("00011100") + 
string("00010100") + 
string("00011100") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 5
bitset<64> (
string("00000000") + 
string("00111000") + 
string("00101000") + 
string("00111000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 5
bitset<64> (
string("00000000") + 
string("01110000") + 
string("01010000") + 
string("01110000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 5
bitset<64> (
string("00000000") + 
string("01100000") + 
string("00100000") + 
string("01100000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 5
bitset<64> (
string("00000000") + 
string("11000000") + 
string("01000000") + 
string("11000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 0 , Y = 6
bitset<64> (
string("00000011") + 
string("00000010") + 
string("00000011") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 1 , Y = 6
bitset<64> (
string("00000000") + 
string("00000100") + 
string("00000110") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 6
bitset<64> (
string("00000000") + 
string("00001010") + 
string("00001110") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 6
bitset<64> (
string("00000000") + 
string("00010100") + 
string("00011100") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 6
bitset<64> (
string("00000000") + 
string("00101000") + 
string("00111000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 6
bitset<64> (
string("00000000") + 
string("01010000") + 
string("01110000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 6
bitset<64> (
string("00000000") + 
string("00100000") + 
string("01100000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 6
bitset<64> (
string("11000000") + 
string("01000000") + 
string("11000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 0 , Y = 7
bitset<64> (
string("00000010") + 
string("00000011") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 1 , Y = 7
bitset<64> (
string("00000101") + 
string("00000111") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 2 , Y = 7
bitset<64> (
string("00001010") + 
string("00001110") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 3 , Y = 7
bitset<64> (
string("00010100") + 
string("00011100") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 4 , Y = 7
bitset<64> (
string("00101000") + 
string("00111000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 5 , Y = 7
bitset<64> (
string("01010000") + 
string("01110000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 6 , Y = 7
bitset<64> (
string("10100000") + 
string("11100000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
),

// Neighbors of X = 7 , Y = 7
bitset<64> (
string("01000000") + 
string("11000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000") + 
string("00000000")
)


};

#endif
